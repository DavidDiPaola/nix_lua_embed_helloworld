/*
2021 David DiPaola.
Licensed under CC0 (https://creativecommons.org/share-your-work/public-domain/cc0/).
*/

#include <stdio.h>

#include <stdlib.h>

#include <lua.h>

#include <lualib.h>  /* for luaL_openlibs() */

#include <lauxlib.h>  /* for luaL functions */

struct {
	lua_State * L;
} _lua;

double
mycfunction(double a, double b) {
	return a + b;
}

/* function signature defined by lua_CFunction */
int
mycfunction_wrapper(lua_State * L) {
	double arg_a = luaL_checknumber(L, 1);
	double arg_b = luaL_checknumber(L, 2);

	double result = mycfunction(arg_a, arg_b);

	lua_pushnumber(L, result);
	return 1;  /* number of results */
}

/* function signature defined by lua_Alloc */
static void *
_lua_alloc(void * ud, void * ptr, size_t osize, size_t nsize) {
	(void)ud;
	(void)osize;

	if (nsize == 0) {
		free(ptr);
		return NULL;
	}
	else {
		return realloc(ptr, nsize);
	}
}

/* function signature defined by lua_CFunction */
static int
_lua_panichandler(lua_State * L) {
	const char * message = lua_tostring(L, -1);
	message = message ? message : "unknown";

	fprintf(stderr, "ERROR: Lua: %s" "\n", message);

	return 0;
}

static void
_lua_init(void) {
	/* for non-custom environments you can use: `lua_State * lua = luaL_newstate();` */
	_lua.L = lua_newstate(_lua_alloc, /*ud=*/NULL);
	if (!_lua.L) {
		fprintf(stderr, "ERROR: Lua: luaL_newstate() failed" "\n");
		exit(1);
	}
	lua_atpanic(_lua.L, _lua_panichandler);

	luaL_openlibs(_lua.L);

	lua_pushcfunction(_lua.L, mycfunction_wrapper);
	lua_setglobal(_lua.L, "mycfunction");
}

static void
_lua_close(void) {
	lua_close(_lua.L);
}

static void
_lua_restart(void) {
	_lua_close();
	_lua_init();
}

static void
_lua_run(const char * program) {
	_lua_restart();

	int status = luaL_dostring(_lua.L, program);
	if (status != LUA_OK) {
		_lua_panichandler(_lua.L);
	}
}

int
main() {
	_lua_init();

	const char * PROGRAMS[] = {
		"print('hello world')"
		,
		"this should not run"
		,
		"for i=0,10,1" "\n"
		"do" "\n"
			"io.write(i .. ' ')" "\n"
		"end" "\n"
		"io.write('\\n')" "\n"
		,
		"print('calling C function... 1 + 2 = ' .. mycfunction(1, 2))"
		,
	};
	#define PROGRAMS_LENGTH sizeof(PROGRAMS)/sizeof(*PROGRAMS)
	for (size_t i=0; i<PROGRAMS_LENGTH; i++) {
		printf("running program %zi..." "\n", i);
		_lua_run(PROGRAMS[i]);
	}

	_lua_close();

	return 0;
}
