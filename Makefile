# 2021 David DiPaola.
# Licensed under CC0 (https://creativecommons.org/share-your-work/public-domain/cc0/).

SRC = main.o
BIN = lua_embed_helloworld

CFLAGS_LUA ?= -I/usr/include/lua5.3
LDFLAGS_LIB_LUA ?= -llua5.3
_CFLAGS = \
	-std=c99 -fwrapv \
	-Wall -Wextra \
	$(CFLAGS_LUA) \
	$(CFLAGS)
_LDFLAGS_LIB = \
	$(LDFLAGS_LIB_LUA) \
	$(LDFLAGS_LIB)

OBJ = $(SRC:.c=.o)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(BIN): $(OBJ)
	$(CC) $(_LDFLAGS) $^ -o $(BIN) $(_LDFLAGS_LIB)
