# nix_lua_embed_helloworld

Example of embedding Lua in a C program

## Prerequisites

- Lua 5.3 development libraries (Debian,Ubuntu: `sudo apt install liblua5.3-dev`)
  - The C and LD flags for this library can be altered via the `CFLAGS_LUA` and `LDFLAGS_LIB_LUA` Make variables
